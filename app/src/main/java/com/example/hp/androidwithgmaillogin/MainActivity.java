/**
 * kalnagin.cse@gmail.com
 */

/**
 * For configuring project visit
 * https://developers.google.com/identity/sign-in/android/start-integrating
 */


package com.example.hp.androidwithgmaillogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
